﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioSwagger.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : Controller
    {

        List<Usuario> Usuarios = new List<Usuario>()
        {
            new Usuario(){Id= 1, NombreApp="APP_BCK", Correo="testapis@tuten.cl", Pass= 1234 }
        };
        [HttpGet]
        public ActionResult<Usuario> Get(int Id)
        {
            var usuario = Usuarios.Where(d => d.Id == Id).FirstOrDefault();

            if (usuario == null) return NotFound();

            return usuario;
        }
    }

    public class Usuario
    {
        public int Id { get; set; }
        public string NombreApp { get; set; }
        public string Correo { get; set; }
        public int Pass { get; set; }
    }
}
